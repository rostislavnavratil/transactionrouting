package com.zentity.wakanda.transactionRouting.controller;

import com.zentity.wakanda.transactionRouting.dto.ClientDto;
import com.zentity.wakanda.transactionRouting.service.ClientService;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;

import javax.inject.Inject;
import java.util.List;

@Controller("/client")
public class ClientController {

    @Inject
    private ClientService clientService;

    @Get("/all")
    public List<ClientDto> getAllClients() {
        return clientService.findAll();
    }

    @Post(value = "/create", processes = MediaType.APPLICATION_JSON)
    public ClientDto createClient(ClientDto clientDto) {
        return clientService.createClient(clientDto);
    }
}
