package com.zentity.wakanda.transactionRouting.repository;

import com.zentity.wakanda.transactionRouting.domain.Client;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {
}
