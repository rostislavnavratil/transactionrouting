package com.zentity.wakanda.transactionRouting.service;

import com.zentity.wakanda.transactionRouting.domain.Client;
import com.zentity.wakanda.transactionRouting.dto.ClientDto;
import com.zentity.wakanda.transactionRouting.mapper.ClientMapper;
import com.zentity.wakanda.transactionRouting.repository.ClientRepository;
import io.micronaut.transaction.annotation.ReadOnly;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Singleton
@Transactional
@ReadOnly
public class ClientService {

    @Inject
    private ClientRepository clientRepository;
    @Inject
    private ClientMapper clientMapper;

    @ReadOnly
    @Transactional
    public List<ClientDto> findAll() {
        List<ClientDto> clientList = new ArrayList<>();
        clientRepository.findAll().forEach(client -> clientList.add(clientMapper.toDto(client)));
        return clientList;
    }

    @Transactional
    public ClientDto createClient(ClientDto clientDto) {
        Client client = clientRepository.save(clientMapper.toEntity(clientDto));
        return clientMapper.toDto(client);
    }
}
