package com.zentity.wakanda.transactionRouting.mapper;

import com.zentity.wakanda.transactionRouting.domain.Client;
import com.zentity.wakanda.transactionRouting.dto.ClientDto;

import javax.inject.Singleton;

@Singleton
public class ClientMapper {

    public Client toEntity(ClientDto clientDto) {
        Client client = Client.builder().firstName(clientDto.getFirstName()).build();
        return client;
    }

    public ClientDto toDto(Client client) {
        ClientDto clientDto = ClientDto.builder().firstName(client.getFirstName()).build();
        return clientDto;
    }
}
