DROP TABLE IF EXISTS CLIENT;

create table client
(
    id         bigserial
        constraint client_pk
            primary key,
    first_name VARCHAR
);

